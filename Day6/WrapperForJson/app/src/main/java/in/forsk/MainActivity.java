package in.forsk;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import in.forsk.wrapper.FacultyWrapper;

public class MainActivity extends Activity {

    private final static String TAG = MainActivity.class.getSimpleName();
    private Context context;

    TextView tv;
    Button btnDownload;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;

        tv = (TextView) findViewById(R.id.textView1);
        btnDownload = (Button) findViewById(R.id.button1);

        btnDownload.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                //Write the code to read the json file from raw folder.
                try {
                    String raw_json = Utils.getStringFromRaw(context, R.raw.faculty_profile_code);

                    showFormattedJson(raw_json);

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void showFormattedJson(String raw_json) {

        ArrayList<FacultyWrapper> facultyWrapperArrayList = Utils.pasreLocalFacultyJson(raw_json);

        StringBuilder sb = new StringBuilder();

        for (FacultyWrapper facultyWrapper : facultyWrapperArrayList) {

            sb.append("#############################");
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getFirst_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getLast_name());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getPhoto());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getDepartment());
            sb.append(System.getProperty("line.separator"));

            sb.append(facultyWrapper.getReserch_area());
            sb.append(System.getProperty("line.separator"));

            ArrayList<String> interest_areas = facultyWrapper.getInterest_areas();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }

            ArrayList<String> contact_details = facultyWrapper.getContact_details();

            for (String s : interest_areas) {
                sb.append(s);
                sb.append(System.getProperty("line.separator"));
            }


        }

        sb.append("#############################");

        tv.setText(sb);


    }


}
